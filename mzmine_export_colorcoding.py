#!/usr/bin/env python

from __future__ import print_function
import argparse, csv, openpyxl, pandas as pd, pyexcelerate, os, sys



COLOR_DETECTED = pyexcelerate.Style(fill = pyexcelerate.Fill(background = pyexcelerate.Color.Color(0, 255, 255)))
COLOR_ESTIMATED = pyexcelerate.Style(fill = pyexcelerate.Fill(background = pyexcelerate.Color.Color(255, 127, 0)))
COLOR_UNKNOWN = pyexcelerate.Style(fill = pyexcelerate.Fill(background = pyexcelerate.Color.Color(255, 255, 0)))


def create_workbook_from_csv(filename):
	wb = None

	with open(filename) as f:
		# Read csv file
		print('Reading data...')		
		df = pd.read_csv(filename, quotechar = '"', skipinitialspace = True, dtype = 'str')


		# Remove last column if unnamed
		col = df.columns.values[-1]

		if 'Unnamed' in col:
			print('Removing extraneous column...')
			df.drop(col, axis = 1, inplace = True)


		# Removing null values
		print('Removing null values...')
		df.fillna('', inplace = True)


		# Open a new workbook
		wb = pyexcelerate.Workbook()


		#
		# Write original data to worksheet
		#

		print('Writing original data to worksheet...')

		ws = wb.new_sheet("Original Data")
		max_col_letter = openpyxl.cell.get_column_letter(len(df.columns))

		# Write header
		ws.range('A1', '%s1' % max_col_letter).value = [ df.columns.values ]

		# Write data
		for i, row in enumerate(df.iterrows()):
			ws.range('A%d' % (i + 2), '%s%d' % (max_col_letter, i + 2)).value = [ row[1].values ]
			print('\r%.2f%%' % (100.0 * i / len(df)), end = '')
		print()


		#
		# Write processed data to worksheet
		#

		# Reduce the data
		cols = [col for col in df.columns.values if 'peak ' not in col]
		peak_cols = [col for col in df.columns.values if 'peak height' in col]

		df2 = df[cols + peak_cols]

		# Write to reduced worksheet
		print('Writing processed data to worksheet...')

		ws = wb.new_sheet("Reduced Data")
		max_col_letter = openpyxl.cell.get_column_letter(len(df2.columns))

		# Write header
		ws.range('A1', '%s1' % max_col_letter).value = [ df2.columns.values ]

		# Write data
		for i, row in enumerate(df2.iterrows()):
			ws.range('A%d' % (i + 2), '%s%d' % (max_col_letter, i + 2)).value = [ row[1].values ]
			print('\r%.2f%%' % (100.0 * (i + 1) / len(df2)), end = '')
		print()

		# Color peak heights
		for i, col in enumerate(peak_cols):
			col_base = col.rstrip(' peak height')

			for j in range(len(df)):
				if df[col_base +' peak status'][j] == 'DETECTED':
					ws.set_cell_style(j + 2, len(cols) + i + 1, COLOR_DETECTED)
				
				if df[col_base +' peak status'][j] == 'ESTIMATED':
					ws.set_cell_style(j + 2, len(cols) + i + 1, COLOR_ESTIMATED)
				
				if df[col_base +' peak status'][j] == 'UNKNOWN':
					ws.set_cell_style(j + 2, len(cols) + i + 1, COLOR_UNKNOWN)

			print('\r%.2f%%' % (100.0 * (i + 1) / len(peak_cols)), end = '')
		print()

		# Format worksheet
		print('Styling worksheet...')
		for i in range(len(df2.columns)):
			ws.set_col_style(i + 1, pyexcelerate.Style(size = max(20, len(df2.columns[i]))))

	return wb 




if __name__ == '__main__':
	# Get arguments from command line
	parser = argparse.ArgumentParser(description = '')
	parser.add_argument('filenames', nargs = '+')
	filenames = parser.parse_args().filenames

	for filename in filenames:
		if os.path.exists(filename):
			basename = filename.rstrip('.csv')

			print('Generating workbook...')
			wb = create_workbook_from_csv(filename)

			if wb is not None:
				print('Exporting to xlsx...')
				wb.save(basename +'.xlsx')
		
		else:
			print(filename +' does not exist')